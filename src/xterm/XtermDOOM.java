/*
 * MochaDOOM on Xterm
 *
 * The MIT License (MIT)
 *
 * Copyright (C) 2022 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package xterm;

import jexer.TApplication;

import jexer.TAction;
import jexer.TApplication;
import jexer.TTimer;
import jexer.backend.ECMA48Terminal;
import jexer.event.TKeypressEvent;
import static jexer.TKeypress.*;

/**
 * Run MochaDOOM from within a terminal emulator.
 */
public class XtermDOOM extends TApplication {

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The desired frames per second.  Due to the use of Thread.sleep() we
     * can't hit this exactly, but it won't be too far off.
     *
     * "Someone" should implement Jexer in a non-GC language.  Hint hint.
     */
    private final int FPS = 30;

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The screen refresh tick timer.
     */
    private TTimer refreshTimer;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param backendType one of the TApplication.BackendType values
     * @throws Exception if TApplication can't instantiate the Backend.
     */
    public XtermDOOM(final BackendType backendType) throws Exception {
        super(backendType);
        getBackend().setTitle("DOOM In An Xterm");

        // Look cute: switch the color theme, window borders, and button
        // styles.
        System.setProperty("jexer.TWindow.borderStyleForeground", "round");
        System.setProperty("jexer.TWindow.borderStyleModal", "round");
        System.setProperty("jexer.TWindow.borderStyleMoving", "round");
        System.setProperty("jexer.TWindow.borderStyleInactive", "round");
        System.setProperty("jexer.TEditColorTheme.borderStyle", "round");
        System.setProperty("jexer.TEditColorTheme.options.borderStyle", "round");
        System.setProperty("jexer.TRadioGroup.borderStyle", "round");
        System.setProperty("jexer.TScreenOptions.borderStyle", "round");
        System.setProperty("jexer.TScreenOptions.grid.borderStyle", "round");
        System.setProperty("jexer.TScreenOptions.options.borderStyle", "round");
        System.setProperty("jexer.TWindow.opacity", "80");
        System.setProperty("jexer.TImage.opacity", "80");
        System.setProperty("jexer.TTerminal.opacity", "80");
        System.setProperty("jexer.TButton.style", "round");
        getTheme().setFemme();
        setDesktop(null);
        setHideStatusBar(true);

        // Add basic menus.
        addToolMenu();
        addFileMenu();
        addWindowMenu();

        // Ensure the screen refreshes periodically.
        refreshTimer = addTimer(1000 / FPS, true,
                new TAction() {
                    public void DO() {
                        doRepaint();
                    }
                }
            );

        // Kick off DOOM.
        new DoomLoggerWindow(this);
        new XtermDoomWindow(this);

    }

    // ------------------------------------------------------------------------
    // Event handlers ---------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // XtermDOOM --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Show FPS.
     */
    @Override
    protected void onPreDraw() {
        if (getScreen() instanceof ECMA48Terminal) {
            ECMA48Terminal terminal = (ECMA48Terminal) getScreen();
            int bytes = terminal.getBytesPerSecond();
            String bps = "";
            if (bytes > 1024 * 1024 * 1024) {
                bps = String.format("%4.2f GB/s",
                    ((double) bytes / (1024 * 1024 * 1024)));
            } else if (bytes > 1024 * 1024) {
                bps = String.format("%4.2f MB/s",
                    ((double) bytes / (1024 * 1024)));
            } else if (bytes > 1024) {
                bps = String.format("%4.2f KB/s",
                    ((double) bytes / 1024));
            } else {
                bps = String.format("%d bytes/s", bytes);
            }
            menuTrayText = String.format("%s FPS %d", bps,
                getFramesPerSecond());
        } else {
            menuTrayText = String.format("FPS %d", getFramesPerSecond());
        }
    }

    // ------------------------------------------------------------------------
    // Main -------------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Main entry point.
     *
     * @param args Command line arguments
     */
    public static void main(final String [] args) {
        try {
            // Swing is the default backend on Windows unless explicitly
            // overridden by jexer.Swing.
            TApplication.BackendType backendType = TApplication.BackendType.XTERM;
            if (System.getProperty("os.name").startsWith("Windows")) {
                backendType = TApplication.BackendType.SWING;
            }
            if (System.getProperty("os.name").startsWith("Mac")) {
                backendType = TApplication.BackendType.SWING;
            }
            if (System.getProperty("jexer.Swing") != null) {
                if (System.getProperty("jexer.Swing", "false").equals("true")) {
                    backendType = TApplication.BackendType.SWING;
                } else {
                    backendType = TApplication.BackendType.XTERM;
                }
            }
            XtermDOOM app = new XtermDOOM(backendType);
            app.run();

            // We exited the application.  Now call System.exit(0) to ensure
            // the running DOOM engine actually shuts down.
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();

            // We are exiting badly.  Now call System.exit(-1) to ensure the
            // running DOOM engine actually shuts down.
            System.exit(-1);
        }

    }

}
