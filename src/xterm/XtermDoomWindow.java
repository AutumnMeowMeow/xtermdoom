/*
 * MochaDOOM on Xterm
 *
 * The MIT License (MIT)
 *
 * Copyright (C) 2022 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
package xterm;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import net.sourceforge.mochadoom.awt.DoomFrame;
import net.sourceforge.mochadoom.doom.event_t;
import net.sourceforge.mochadoom.doom.evtype_t;
import net.sourceforge.mochadoom.doom.CommandLine;
import net.sourceforge.mochadoom.doom.DoomMain;
import net.sourceforge.mochadoom.doom.ICommandLineManager;
import net.sourceforge.mochadoom.game.Keys;
import net.sourceforge.mochadoom.menu.IVariablesManager;
import net.sourceforge.mochadoom.menu.VarsManager;
import net.sourceforge.mochadoom.system.BppMode;

import jexer.TApplication;
import jexer.TExceptionDialog;
import jexer.TImage;
import jexer.TWindow;
import jexer.backend.ECMA48Terminal;
import jexer.bits.CellAttributes;
import jexer.event.TKeypressEvent;
import jexer.event.TResizeEvent;
import static jexer.TKeypress.*;

/**
 * Display a DOOM engine and route keypresses and mouse events to it.
 */
public class XtermDoomWindow extends TWindow {

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The global instance (AAAAAHHH!) of this window.
     */
    private static XtermDoomWindow instance = null;

    /**
     * The DOOM engine running the game.
     */
    private DoomMain engine;

    /**
     * The image showing on screen.
     */
    private TImage image;

    /**
     * The DoomFrame generating the screen.
     */
    private DoomFrame doomFrame;

    /**
     * The last normal ASCII character sent to the engine.
     */
    private int lastCh = -1;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param application TApplication that manages this window
     */
    public XtermDoomWindow(final TApplication application) {

        super(application, "DOOM", 0, 0, 60, 15, CENTERED | RESIZABLE);

        if (instance != null) {
            // Only only instance allowed.
            remove();
            return;
        }
        instance = this;
        setWidth((320 / getScreen().getTextWidth()) + 3);
        setHeight((200 / getScreen().getTextHeight()) + 3);

        image = addImage(0, 0, getWidth() - 2, getHeight() - 2,
            new BufferedImage(320, 200, BufferedImage.TYPE_INT_ARGB), 0, 0);
        // Dynamically scale to the window size, keeping the aspect ratio.
        image.setScaleType(TImage.Scale.SCALE);

        // Performance tweaks -------------------------------------------------

        // Disable anti-aliasing.
        image.setAntiAlias(false);

        // Disable translucence for this window.
        setAlpha(255);

        // Performance tweaks -------------------------------------------------

        // Instantiate DOOM!
        try {
            /*
             * Set the command line parameters.  I pick the wad and
             * deliberately tell the engine NOT to scale when rendering:
             * Jexer will scale on final output.  I also disable sound.
             */
            ICommandLineManager clm = new CommandLine(new String [] {
                    "-iwad", "doom1.wad", "-multiply", "1", "-nosound"});

            // Handles variables and settings from default.cfg
            IVariablesManager vm = new VarsManager(clm);

            // Load before initing other systems, but don't apply them yet.
            DoomLoggerWindow.log("M_LoadDefaults: Load system defaults.");
            vm.LoadDefaults(vm.getDefaultFile());

            // Mocha Doom has a web of static variables. :-( Select Indexed
            // (8-bit).  It's faster and looks much better.
            net.sourceforge.mochadoom.system.Main.bpp = BppMode.Indexed;
            engine = new DoomMain.Indexed();
            engine.setCommandLineArgs(clm);
            engine.registerVariableManager(vm);

            Runnable gameLoop = new Runnable() {
                public void run() {
                    try {
                        engine.Init();
                        engine.Start();
                    } catch (Exception e) {
                        new TExceptionDialog(getApplication(), e);
                        // e.printStackTrace();
                        // System.exit(-1);
                    }
                }
            };
            (new Thread(gameLoop)).start();
        } catch (Exception e) {
            /*
             * I would normally put up a nice dialog, but chances are Mocha
             * Doom has spewed tons of stuff to stderr anyway.  Let's go with
             * the flow...
             */
            new TExceptionDialog(getApplication(), e);
            // e.printStackTrace();
            // System.exit(-1);
        }
    }

    // ------------------------------------------------------------------------
    // Event handlers ---------------------------------------------------------
    // ------------------------------------------------------------------------

    @Override
    public void onResize(final TResizeEvent event) {
        if (event.getType() == TResizeEvent.Type.WIDGET) {
            TResizeEvent imageSize;
            imageSize = new TResizeEvent(event.getBackend(),
                TResizeEvent.Type.WIDGET, event.getWidth() - 2,
                event.getHeight() - 2);
            image.onResize(imageSize);
        }
    }

    /**
     * Shutdown the engine.
     */
    protected void onClose() {
        if (engine != null) {
            super.onClose();
            // Mocha Doom expects to have exactly one instance and calls
            // System.exit() ! when the game is over.  Sigh.

            engine.I.Quit();
        }
        engine = null;
    }

    /**
     * Handle keystrokes.
     *
     * @param keypress keystroke event
     */
    @Override
    public void onKeypress(final TKeypressEvent keypress) {

        if (!keypress.getKey().isFnKey()
            && !keypress.getKey().isAlt()
            && !keypress.getKey().isCtrl()
        ) {
            // Plain old keystroke, process it
            int ch = keypress.getKey().getChar();
            if (lastCh == ch) {
                // Repeated key, just leave it.
                return;
            }

            if (lastCh != -1) {
                engine.PostEvent(new event_t(evtype_t.ev_keyup, lastCh, 0, 0));
            }
            engine.PostEvent(new event_t(evtype_t.ev_keydown, ch, 0, 0));
            lastCh = ch;
        }

        if (keypress.equals(kbEsc)) {
            engine.PostEvent(new event_t(evtype_t.ev_keydown,
                    Keys.KEY_ESCAPE, 0, 0));
            engine.PostEvent(new event_t(evtype_t.ev_keyup,
                    Keys.KEY_ESCAPE, 0, 0));
            lastCh = -1;
            return;
        }
        if (keypress.equals(kbTab)) {
            engine.PostEvent(new event_t(evtype_t.ev_keydown,
                    Keys.KEY_TAB, 0, 0));
            engine.PostEvent(new event_t(evtype_t.ev_keyup,
                    Keys.KEY_TAB, 0, 0));
            lastCh = -1;
            return;
        }
        if (keypress.equals(kbUp)) {
            engine.PostEvent(new event_t(evtype_t.ev_keydown,
                    Keys.KEY_UPARROW, 0, 0));
            lastCh = Keys.KEY_UPARROW;
            return;
        }
        if (keypress.equals(kbDown)) {
            engine.PostEvent(new event_t(evtype_t.ev_keydown,
                    Keys.KEY_DOWNARROW, 0, 0));
            lastCh = Keys.KEY_DOWNARROW;
            return;
        }
        if (keypress.equals(kbLeft)) {
            engine.PostEvent(new event_t(evtype_t.ev_keydown,
                    Keys.KEY_LEFTARROW, 0, 0));
            lastCh = Keys.KEY_LEFTARROW;
            return;
        }
        if (keypress.equals(kbRight)) {
            engine.PostEvent(new event_t(evtype_t.ev_keydown,
                    Keys.KEY_RIGHTARROW, 0, 0));
            lastCh = Keys.KEY_RIGHTARROW;
            return;
        }
        if (keypress.equals(kbEnter)) {
            engine.PostEvent(new event_t(evtype_t.ev_keydown,
                    Keys.KEY_ENTER, 0, 0));
            lastCh = Keys.KEY_ENTER;
            return;
        }


    }

    // ------------------------------------------------------------------------
    // XtermDoomWindow --------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Draw the window.
     */
    @Override
    public void draw() {
        if (doomFrame != null) {
            setTitle(String.format("%s - %dx%d", doomFrame.title,
                    image.getWidth() * getScreen().getTextWidth(),
                    image.getHeight() * getScreen().getTextHeight()));
        }

        // TWindow draws the window border, title, and background.
        super.draw();

        if (doomFrame != null) {
            BufferedImage frame = doomFrame.getScreen();
            if (frame != null) {
                image.setImage(frame, false);
            }
        }
    }

    /**
     * Get the static instance.
     *
     * @return the static instance
     */
    public static XtermDoomWindow getInstance() {
        return instance;
    }

    /**
     * Set the DoomFrame instance that is generating the screen.
     *
     * @param doomFrame the DoomFrame
     */
    public void setDoomFrame(final DoomFrame doomFrame) {
        this.doomFrame = doomFrame;
    }

}
