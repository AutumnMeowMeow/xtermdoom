# XtermDOOM

XtermDOOM is a combination of [Mocha
Doom](https://github.com/jendave/mochadoom) and
[Jexer](https://gitlab.com/AutumnMeowMeow/jexer) which permits playing
DOOM inside a terminal emulator:

![Video of First Working Screens](screenshots/first_post.mp4)

![Video of First Official Kill](screenshots/first_kill.mp4)

## License

This combined work is licensed under the GNU Public License version 3
or later:

* XtermDOOM is a derivative work of Mocha Doom by Victor Epitropou,
  Alexandre-Xavier Labonté-Lamoureux, and Good Sign, which is licensed
  under the GNU Public License version 3 or later.

* Mocha Doom is a derivative work of DOOM by id Software, Inc., which
  is licensed under the GNU Public Licenser version 2 or later.

* Files added by Autumn Lamonte are licensed MIT.  These are
  build.xml, and all files under src/xterm.



## Terminal Requirements

The terminal requirements are:

* Either sixel or iTerm2 graphics support.

* SGR-Pixels mouse encoding (1016).

* Any-event mouse reporting (1003).



## Running It

Build via `ant`, or toss into an IDE.

Run it by opening your Xterm, and:

```
$ cp /path/to/doom1.wad .
$ unset DISPLAY
$ java -jar xtermdoom.jar 2>/dev/null
```

It is not really working keyboard or mouse input.  But when it does,
the following is expected:

* Keyboard events will be continuous movement until the next key.

* Play with the mouse, press spacebar to open doors and press buttons,
  and use mouse wheel up/down to switch weapons.

There is no sound.

Now go kill stuff, very shakily.



## Project Goals

* Be able to instantiate multiple instances of the DOOM engine, and
  start/pause/stop each from outside the black box.

* A simple interface from the outside to each DOOM engine: initialize
  with arguments, accept keyboard/mouse events, and support both
  push/pull to obtain a BufferedScreen image of the game screen.

* Retrofit fixes from more recent Mocha Doom derivatives to be able to
  play stock DOOM wads.

* Run fully in headless mode.

* Run without filesystem access.

* Be a reasonable starting point for porting DOOM to other languages.



## Explicitly NOT Goals

* Be a replica of the DOS-based DOOM experience.



## Roadmap

* Remove AWT dependencies.

* Add keyboard and mouse inputs from Jexer.

* Eliminate use of ~~System.exit() (done)~~ and System.out/err within
  DOOM engine.

* Remove all dead code.

* Remove generics / static instance constructors.
